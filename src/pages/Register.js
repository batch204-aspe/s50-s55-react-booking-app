import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register(props){

	// console.log(props);

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	const { user, setUser } = useContext(UserContext);

	/*
	To properly change and save input values, we must implement two-way binding
	We need to capture whatever the user types in the input as they are typing
	Meaning we need the input's .value
	To get the .value, we capture the event (in this case, onChange). The target of the onChange event is the input, meaning we can get the .value
	*/

	useEffect(() => {
		// console.log(email);
		// console.log(password1);
		// console.log(password2);

		// if fields are populated, mobile number is exactly 11 characters and passwords match
		if (( firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2) ) {
			setIsActive(true);

		} else {
			setIsActive(false);

		}

	}, [firstName, lastName, email, mobileNo, password1, password2] )

	function registerUser(e){

		e.preventDefault() // prevent default behaviour, so that the form does not submit

        /* Uncomment this code later
        	this is for redirect to Homepage after registration
        	
		localStorage.setItem('email', email);

		setUser({
			email: email
		});
        */

		// User Registration
			// 1st Check if the user email is existing
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);
				// Check the condition if exist
			if(data) { // if TRUE alert("Duplicate email exists")
				alert("Duplicate email exists");
			} else { // if FALSE POST the FORM user registration
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data) {
						alert("Successfully Registered");
						// redirect the user to the login page
						props.history.push("/login");
					} else {
						alert("Something went wrong");
					}
				})
			}
		});

		// setEmail("");
		// setPassword1("");
		// setPassword2("");
		// alert('Thank you for registering');
	}

	return(
		(user.id !== null) ?
			<Redirect to="/" />
		:
		<Form onSubmit={e => registerUser(e)}>

			<Form.Group className="mt-3" controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your first Name"
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mt-3" controlId="LastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your Last Name"
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mt-3" controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group className="mt-3" controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your Mobile Number"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mt-3" controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mt-3" controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{/*	Conditional Rendering

				Ternary Operator

				(condition) ? true : false

			*/}

			{ isActive ? 
				<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button className="mt-3" variant="primary"  id="submitBtn" disabled>
					Submit
				</Button>
			}
		</Form>
	)
}
