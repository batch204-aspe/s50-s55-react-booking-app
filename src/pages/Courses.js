import { useEffect, useState, useContext } from 'react';
// import courseData from '../data/courseData';
import CourseCard from "../components/CourseCard";
import AdminView from "../components/AdminView";
import UserContext from '../UserContext';


export default function Courses () {

	const [coursesData, setCoursesData] = useState([]);

	const { user } = useContext(UserContext)
	console.log(user);

	// Check to see if the mock data was captured
	// console.log(courseData);
	// console.log(courseData[0])


	// Props
		// is a shorthand for "property" since components are considered as object in ReactJS

		// Props is a way to pass data from a parent component to a child component.

		// It is synonymous to the function parameter.
		// This is reffered to as "props drilling".

		// pass data from parent to child

	const fetchData = () => {

		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {
			// console.log(data);
			setCoursesData(data);
		});

	}
	
	// GEt Request
	useEffect(() => {

		// console.log(process.env.REACT_APP_API_URL);
		// changes to env files are applied ONLY at build time (when starting the project locally)

		// Fetch by default always makes a GET request,unless a different one is specified
		// Always add Fetch request for getting data in a useEffect Hook
		fetchData();

	}, [] );

	const courses = coursesData.map(course => {
		if(course.isActive){
			return (
				<CourseCard courseProp={course} key={course._id} />
			);
		} else {
			return null
		}

		
	});



	return (
		(user.isAdmin) ?
		<AdminView coursesProp={coursesData} fetchData={fetchData} />
		:
		<>
			<h1>Courses</h1>
			{courses}
		</>
	);
}