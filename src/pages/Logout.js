import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout () {

	// get the SetUser SETTER from App.js
	const { setUser } = useContext(UserContext);

	// clear localStorage
	localStorage.clear();

	// When the component mounts/page loads, set the user state to null
	useEffect(() => {

		setUser({
			id: null,
			isAdmin: null
		});
	});

	return (
		// Redirects the user to login page or whenever ROutes you want
		<Redirect to="/login" />
	);
}