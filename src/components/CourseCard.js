import { useState, useEffect } from 'react';
import {Button, Row, Col, Card} from 'react-bootstrap';
import { Link } from 'react-router-dom';


// Destructuring is done in the parameter to retrieve the courseProp
export default function CourseCard({courseProp}) {

    // console.log(props.courseProp);
    // console.log(courseProp);

    const { _id, name, description, price} = courseProp;

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
        // SYntax:
            // const [getter, setter] = useState(initialGetterValue)

    //  when a component mounts (loads for the first time), any associated states will undergo a state change from null to the given initial/default state
        // e.g. count below goes from null to 0, since 0 is our initial state
    /*const [count, setCount] = useState(0);*/
    // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element.
    /*console.log(useState(0));*/
    //  function that keeps track of the enrollees for a course
    //  By default JavaScript is synchronous, as it executes code from the top of the file all the way to the bottom and will wait for the completion of one expression before it proceeds to the next
    //  The setter function for useStates are asynchronous, allowing it to execute separately from other codes in the program
    //  the "setCount" function is being executed while the console.log is already being completed resulting in the console to be behind by one count.

    // si getter naman is to view yung value nung use state po natin, while si setter natin is to change po yung initial value sa may loob nung useState() natin.

    /* Activity S51
    1. Create a seats state in the CourseCard component and set the initial value to 30.
    2. For every enrollment, deduct one to the seats.
    3. If the seats reaches zero do the following:
        - Do not add to the count state.
        - Do not deduct to the seats state.
        - Show an alert that says No more seats available.
    4. Create a condition that will check if the "seats" is "0" alert the user of no more courses



    */
   /* const [seats, setSeatsCount] = useState(10);

    function enroll () {
        setCount(count + 1);
        setSeatsCount(seats - 1);*/

        /*
        if (count === 30 && seats === 0) {
            setCount(count);
            setSeatsCount(seats);
            alert("No more seats available");
        }
        
        console.log(`Enrollees: ${count}`);
        console.log(`Seats: ${seats}`);
        
    } */

    /* Activity s51 Other Solutions 

        if (seats > 0){
            setCount (count + 1);
            console.log(`Enrollees: ${count}`);
            setSeatsCount(seats - 1);
            console.log(`Seats: ${seats}`);
        } else {
            alert("No more seats available")
        }

    */

    // 1. useEffect makes any given code block happen when a state changes AND when a component first mounts (such as on initial page load )

    /* Syntax:

        useEffect (() => {
            code to be executed
        }, [ state(s) to monitor ] )

    */

    // 2. In the example below, since count and seats are in the array of our useState, the code block will execute whenver those states change

    // 3. if the array is blank, the code will b executed on component mount ONLY

    // 4. Do NOT omit the array completely
/*
    useEffect(() => {
        if(seats === 0){
            alert('No more seats available')
        }
    }, [count, seats])
    */

	return (

		 <Row className="my-3">
            <Col>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>

                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>PhP {price}</Card.Text>
                        {/*<Card.Text>Enrollees: {count}</Card.Text>
                        <Card.Text>Seats: {seats}</Card.Text>
                        <Button variant="primary" onClick={enroll}>Enroll</Button>*/}
                        <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
                    </Card.Body>
                </Card>
            </Col>
        </Row>

	);
}